<%@page import="com.uaa.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Info</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <%
            User user = (User) request.getAttribute("user");
        %>
        <h1>User Info</h1>
        <table>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Birthday</th>
                <th>Password</th>
                <th>Gender</th>   
            </tr>
            <tr>
                <td><%=user.getFirstName()%></td>
                <td><%=user.getLastName()%></td>
                <td><%=user.getBirthDay()%></td>
                <td><%=user.getPassword()%></td>
                <td><%=user.getGender()%></td>
            </tr>
        </table>
    </body>
</html>
