<%-- 
    Document   : index
    Created on : 05-oct-2017, 01:50:15
    Author     : Mario1
--%>

<%@page import="com.uaa.controller.LoginController"%>
<%@page import="java.util.Date"%>
<%@page session="false"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Session</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, 
              initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <div class="logout-container">
            <a class="logout" href="logout">Logout</a>
        </div>
        <form action="login" method="POST">
            <%
                HttpSession _session = request.getSession(false);
                boolean error = false;
                String message = "";
                String lastLogin = "";

                if (_session != null && _session
                        .getAttribute(LoginController.LAST_LOGIN) 
                        != null) {
                    lastLogin = _session
                            .getAttribute(LoginController.LAST_LOGIN)
                            .toString();
                }

                if (request.getAttribute(LoginController.ERROR) != null) {
                    error = (Boolean) request
                            .getAttribute(LoginController.ERROR);
                }

                if (request.getAttribute(LoginController.MESSAGE) != null) {
                    message = (String) request
                            .getAttribute(LoginController.MESSAGE);
                }
            %>
            <span class="<%=error ? "error" : "success"%>">
                <%=message + " " + lastLogin%>
            </span>
            <div class="img-container">
                <img src="img/avatar.png" alt="Avatar" class="avatar">
            </div>
            <div class="container">
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter Username" 
                       name="username" required>

                <label><b>Password</b></label>
                <input type="password" placeholder="Enter Password" 
                       name="password" required>

                <button type="submit">Login</button>
                <input type="checkbox" checked="checked"/> Remember me
            </div>

        </form>
    </body>
</html>
