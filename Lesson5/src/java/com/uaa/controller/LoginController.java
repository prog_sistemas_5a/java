/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller;

import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mario1
 */
@WebServlet(name = "LoginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {

    private static final String USERNAME = "username";
    private static final String PSWD = "password";
    private static final String DISPATCH_TO = "index.jsp";
    public static final String LAST_LOGIN = "last_login";
    public static final String ERROR = "error";
    public static final String MESSAGE = "message";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = request
                .getRequestDispatcher(DISPATCH_TO);
        HttpSession session = request.getSession(false); //if true this would create a session

        boolean error = true;
        String message;
        Date lastLogin = null;
        String usr = request.getParameter(USERNAME);
        String pswd = request.getParameter(PSWD);

        if (session == null) {
            if (usr != null && pswd != null 
                    && usr.equals("admin") && pswd.equals("admin")) {
                message = "you are logged in!";
                error = false;
                session = request.getSession();
                lastLogin = new Date();
                session.setAttribute(LAST_LOGIN, lastLogin);
            } else {
                error = true;
                message = "invalid credentials";
            }
        } else {
            message = "you are logged in!";
            error = false;
            lastLogin = new Date();
            session.setAttribute(LAST_LOGIN, lastLogin);
        }

        request.setAttribute(ERROR, error);
        request.setAttribute(MESSAGE, message);

        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
