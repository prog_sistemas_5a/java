package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.uaa.controller.LoginController;
import java.util.Date;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Session</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, \n");
      out.write("              initial-scale=1.0\">\n");
      out.write("        <link href=\"css/styles.css\" rel=\"stylesheet\" />\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"logout-container\">\n");
      out.write("            <a class=\"logout\" href=\"logout\">Logout</a>\n");
      out.write("        </div>\n");
      out.write("        <form action=\"login\" method=\"POST\">\n");
      out.write("            ");

                HttpSession _session = request.getSession(false);
                boolean error = false;
                String message = "";
                String lastLogin = "";

                if (_session != null && _session
                        .getAttribute(LoginController.LAST_LOGIN) 
                        != null) {
                    lastLogin = _session
                            .getAttribute(LoginController.LAST_LOGIN)
                            .toString();
                }

                if (request.getAttribute(LoginController.ERROR) != null) {
                    error = (Boolean) request
                            .getAttribute(LoginController.ERROR);
                }

                if (request.getAttribute(LoginController.MESSAGE) != null) {
                    message = (String) request
                            .getAttribute(LoginController.MESSAGE);
                }
            
      out.write("\n");
      out.write("            <span class=\"");
      out.print(error ? "error" : "success");
      out.write("\">\n");
      out.write("                ");
      out.print(message + " " + lastLogin);
      out.write("\n");
      out.write("            </span>\n");
      out.write("            <div class=\"img-container\">\n");
      out.write("                <img src=\"img/avatar.png\" alt=\"Avatar\" class=\"avatar\">\n");
      out.write("            </div>\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <label><b>Username</b></label>\n");
      out.write("                <input type=\"text\" placeholder=\"Enter Username\" \n");
      out.write("                       name=\"username\" required>\n");
      out.write("\n");
      out.write("                <label><b>Password</b></label>\n");
      out.write("                <input type=\"password\" placeholder=\"Enter Password\" \n");
      out.write("                       name=\"password\" required>\n");
      out.write("\n");
      out.write("                <button type=\"submit\">Login</button>\n");
      out.write("                <input type=\"checkbox\" checked=\"checked\"/> Remember me\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
