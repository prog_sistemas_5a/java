<%-- 
    Document   : today
    Created on : 04-oct-2017, 8:46:59
    Author     : Mario1
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            String name = "Mario";
            Date today = new Date();
        %>
        <h1>Hello World <%=name %> today is <%=today.toString() %></h1>
    </body>
</html>
