package com.uaa.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.uaa.model.TodoItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mario1
 */
@WebServlet(urlPatterns = {"/todo"})
public class TodoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(
            HttpServletRequest request, 
            HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = request
         .getRequestDispatcher("view/todo/todo.jsp");
        Random rand = new Random();
        List<TodoItem> todoList = new ArrayList<>();
        String[] items = new String[]{
            "Wake up", "Shower", "Eat Brakefast",
            "Drive", "Checkin", "Teach",
            "Checkout", "Go to work", "Work",
            "Eat", "Work", "Drive home",
            "Go to the Gym", "Eat dinner", "Shower",
            "Work", "Sleep"
        };
        String[] descriptions = new String[]{
            "Get out of your bed", "Clean yourself", 
            "Most important taks of the day",
            "Don't crash", "You won't get paid otherwise", 
            "", "Don't forget!", 
            "Many meetings and bugs to do!", "",
            "Taco time!", "", "",
            "", "", "You are all sweaty..",
            "Just think in the money", "Finally is over.."
        };
        TodoItem todoItem;
        for(int i=0; i<items.length; i++) {
            todoItem = new TodoItem();
            String item = items[i];
            String description = descriptions[i];
            todoItem.setTask(item);
            todoItem.setDescription(description);
            todoItem.setCompleted(rand.nextBoolean());
            todoList.add(todoItem);
        }
        request.setAttribute("todoList", todoList);
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
