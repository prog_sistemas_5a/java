<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>EL Expressions</h1>
        <p>${1 > (4/2)}</p>
        <p>${100.0 == 100}</p>
        <p>${(10*10) ne 100}</p>
        <p>${'a' < 'b'}</p>
        <p>${'hip' gt 'hit'}</p>
        <p>${4 > 3}</p>
        <p>${1.2E4 + 1.4}</p>
        <p>${3 div 4}</p>
        <p>${10 mod 4}</p>
        <p>${!empty param.Add}</p>
        <p>${header["host"]}</p>
    </body>
</html>
