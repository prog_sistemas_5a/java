/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mario1
 */
@Entity
@Table(name = "todo_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TodoItem.findAll", query = "SELECT t FROM TodoItem t")
    , @NamedQuery(name = "TodoItem.findById", query = "SELECT t FROM TodoItem t WHERE t.id = :id")
    , @NamedQuery(name = "TodoItem.findByTask", query = "SELECT t FROM TodoItem t WHERE t.task = :task")
    , @NamedQuery(name = "TodoItem.findByDescription", query = "SELECT t FROM TodoItem t WHERE t.description = :description")
    , @NamedQuery(name = "TodoItem.findByCompleted", query = "SELECT t FROM TodoItem t WHERE t.completed = :completed")})
public class TodoItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "task")
    private String task;
    @Column(name = "description")
    private String description;
    @Column(name = "completed")
    private Short completed;

    public TodoItem() {
    }

    public TodoItem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getCompleted() {
        return completed;
    }

    public void setCompleted(Short completed) {
        this.completed = completed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TodoItem)) {
            return false;
        }
        TodoItem other = (TodoItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uaa.entity.TodoItem[ id=" + id + " ]";
    }
    
}
