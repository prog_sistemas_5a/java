/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.bean.helper;

import com.uaa.entity.TodoItem;
import com.uaa.jpa.TodoItemJpaController;
import com.uaa.jpa.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Mario1
 */
public class JBTodoItemManager {

    private static JBTodoItemManager instance;
    TodoItemJpaController jpaController;

    private JBTodoItemManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Lesson8PU");
        this.jpaController = new TodoItemJpaController(emf);
    }

    public static JBTodoItemManager getInstance() {
        if (instance == null) {
            instance = new JBTodoItemManager();
        }
        return instance;
    }

    public void create(TodoItem item) {
        this.jpaController.create(item);
    }

    public List<TodoItem> read() {
        return this.jpaController.findTodoItemEntities();
    }

    public void update(TodoItem item) {
        try {
            this.jpaController.edit(item);
        } catch (Exception ex) {
            Logger.getLogger(JBTodoItemManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(int id) {
        try {
            this.jpaController.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(JBTodoItemManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
