<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP and Java Conditionals</title>
    </head>
    <body>
        <h1>Java Conditionals</h1>
        <%
            int a = 10, b = 30;
        %>
        <p> a == b ? <%= (a == b)%></p>
        <p> a > b ? <%= (a > b)%></p>
        <p> a < b ? <%= (a < b)%></p>
        <p> a >= b ? <%= (a >= b)%></p>
        <p> a <= b ? <%= (a <= b)%></p>
        <%
            String name = "";
            if (name.isEmpty()) {
        %>
        <p>Greetings stranger!</p>
        <%
        } else {
        %>
        <p>Hello <%=name%>, how are you?</p>
        <%
            }
        %>

    </body>
</html>
