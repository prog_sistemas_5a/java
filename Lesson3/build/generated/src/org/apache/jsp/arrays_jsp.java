package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Arrays;

public final class arrays_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP and Java Arrays</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");

            int[] array = new int[]{
                1, 4, 10, 111, 20, 2, 5, 90, 3, 8
            };
        
      out.write("\n");
      out.write("        <h1>Java Arrays</h1>\n");
      out.write("        <p>array = ");
      out.print(Arrays.toString(array));
      out.write("</p>\n");
      out.write("        <p>array has ");
      out.print(array.length);
      out.write(" items</p>\n");
      out.write("        ");

            Arrays.sort(array);
        
      out.write("\n");
      out.write("        <p>array sorted = \n");
      out.write("            ");
      out.print(Arrays.toString(array));
      out.write("</p>\n");
      out.write("            ");

                int max = array[array.length - 1];
                int min = array[0];
            
      out.write("\n");
      out.write("        <p>max number is: ");
      out.print(max);
      out.write("</p>\n");
      out.write("        <p>min number is: ");
      out.print(min);
      out.write("</p>\n");
      out.write("        ");

            String[] groceries = new String[]{
                "Milk", "Eggs", "Meat",
                "Bread", "Beans", "Tortilla"
            };
        
      out.write("\n");
      out.write("        <p>Groceries I need to buy: </p>\n");
      out.write("        <ul>\n");
      out.write("            ");
 for (String grocery : groceries) {
      out.write("\n");
      out.write("            <li>");
      out.print(grocery);
      out.write("</li>\n");
      out.write("                ");
 }
      out.write("\n");
      out.write("        </ul>\n");
      out.write("\n");
      out.write("        ");

            float[] prices = new float[]{
                10.5f, 8f, 2.3f, 4.2f, 5.3f, 10.3f
            };
        
      out.write("\n");
      out.write("        <p>Groceries I need to buy with prices: </p>\n");
      out.write("        <table>\n");
      out.write("            <tr>\n");
      out.write("                <th>Grocery</th>\n");
      out.write("                <th>Price</th>\n");
      out.write("            </tr>\n");
      out.write("            ");
for (int i = 0;
                        i < groceries.length; i++) {
      out.write("\n");
      out.write("            <tr>\n");
      out.write("                <td>");
      out.print(groceries[i]);
      out.write("</td>\n");
      out.write("                <td>");
      out.print(prices[i]);
      out.write("</td> \n");
      out.write("            </tr>\n");
      out.write("            ");
 }
      out.write("\n");
      out.write("        </table>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
