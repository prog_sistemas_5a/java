<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP and Java Operators</title>
    </head>
    <body>
        <h1>Operators in java</h1>
        <%
            int a, b;
            a = 10;
            b = 30;
        %>
        <p>Value of a = <%=a%></p>
        <p>Value of b = <%=b%></p>
        <p>Value of ++b = <%=++b%></p>
        <p>Value of --b = <%=--b%></p>
        <p>Value of a + b = <%=a + b%></p>
        <p>Value of a - b = <%=a - b%></p>
        <p>Value of a * b = <%=(a * b)%></p>
        <p>Value of b / a = <%=(b / a)%></p>
        <p>Value of true AND false = <%=(true && false)%></p>
        <p>Value of true OR false = <%=(true || false)%></p>
        <%
            String name = "James";
            boolean result = name instanceof String;
        %>
        <p>is "james" an String? <%out.print(result);%></p>
    </body>
</html>
