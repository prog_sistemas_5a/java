<%-- 
    Document   : loops
    Created on : 01-oct-2017, 21:43:17
    Author     : Mario1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP and Java Loops</title>
    </head>
    <body>
        <h1>Java Loops</h1>
        <% int times = 2;
            while (times-- > 0) {
        %>
        <p>while</p>
        <% }%>
        
        <% times = 3;
            do {
        %>
        <p>do while</p>
        <%
            } while (times-- > 0);
        %>
        
        <% times = 10;
            for (int i=0; i<times; i++) {
        %>
        <p>for</p>
        <%
            }
        %>
    </body>
</html>
