<%@page import="java.util.Arrays"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP and Java Arrays</title>
    </head>
    <body>
        <%
            int[] array = new int[]{
                1, 4, 10, 111, 20, 2, 5, 90, 3, 8
            };
        %>
        <h1>Java Arrays</h1>
        <p>array = <%=Arrays.toString(array)%></p>
        <p>array has <%=array.length%> items</p>
        <%
            Arrays.sort(array);
        %>
        <p>array sorted = 
            <%=Arrays.toString(array)%></p>
            <%
                int max = array[array.length - 1];
                int min = array[0];
            %>
        <p>max number is: <%=max%></p>
        <p>min number is: <%=min%></p>
        <%
            String[] groceries = new String[]{
                "Milk", "Eggs", "Meat",
                "Bread", "Beans", "Tortilla"
            };
        %>
        <p>Groceries I need to buy: </p>
        <ul>
            <% for (String grocery : groceries) {%>
            <li><%=grocery%></li>
                <% }%>
        </ul>

        <%
            float[] prices = new float[]{
                10.5f, 8f, 2.3f, 4.2f, 5.3f, 10.3f
            };
        %>
        <p>Groceries I need to buy with prices: </p>
        <table>
            <tr>
                <th>Grocery</th>
                <th>Price</th>
            </tr>
            <%for (int i = 0;
                        i < groceries.length; i++) {%>
            <tr>
                <td><%=groceries[i]%></td>
                <td><%=prices[i]%></td> 
            </tr>
            <% }%>
        </table>

    </body>
</html>
