<%-- 
    Document   : update_task
    Created on : 22-oct-2017, 16:25:12
    Author     : Mario1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Task</title>
    </head>
    <body>
        <h1>Update Task</h1>
        <p><a href="index.html">Return</a></p>
        <form action="todo" method="POST">
            Id: <input name="id" type="number" />
            Task: <input name="task" /><br/>
            Description: <input name="description" /><br/>
            Completed: 
            <input name="completed" type="checkbox" /><br/>
            <button>Update</button>
            <input name="action" type="hidden" value="update" />
        </form>
    </body>
</html>
