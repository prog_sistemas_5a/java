<%-- 
    Document   : delete_task
    Created on : 22-oct-2017, 16:29:38
    Author     : Mario1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Delete Task</h1>
        <p><a href="index.html">Return</a></p>
        <form action="todo" method="POST">
            id: <input name="id" /><br/>
            <button>Delete</button>
            <input name="action" type="hidden" value="delete" />
        </form>
    </body>
</html>
