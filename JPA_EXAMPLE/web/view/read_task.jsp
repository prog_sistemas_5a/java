<%-- 
    Document   : read_task
    Created on : 22-oct-2017, 16:25:02
    Author     : Mario1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/styles.css" rel="stylesheet" />
        <title>Todo List</title>
    </head>
    <body>
        <h1>Stuff To Do</h1>
        <p><a href="index.html">Return</a></p>
        <c:if test="${not empty todoList}">
            <table>
                <tr>
                    <th>Id</th>
                    <th>Task</th>
                    <th>Description</th>
                    <th>Completed?</th>
                </tr>
                <c:forEach var="todoItem" items="${todoList}">
                    <tr>
                        <td>${todoItem.id}</td>
                        <td>${todoItem.task}</td>
                        <td>${todoItem.description}</td>
                        <td>
                            <input type="checkbox" 
                                   <c:if test="${todoItem.completed}">
                                       checked
                                   </c:if> 
                            />
                        </td>
                    </tr>
                </c:forEach>
            </table> 
        </c:if>
    </body>
</html>

