<%-- 
    Document   : CreateTask
    Created on : 22-oct-2017, 16:07:47
    Author     : Mario1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Task</title>
    </head>
    <body>
        <h1>Create Task</h1>
        <p><a href="index.html">Return</a></p>
        <form action="todo" method="POST">
            Task: <input name="task" /><br/>
            Description: <input name="description" /><br/>
            Completed: <input name="completed" type="checkbox" /><br/>
            <button>Create</button>
            <input name="action" type="hidden" value="create" />
        </form>
    </body>
</html>
