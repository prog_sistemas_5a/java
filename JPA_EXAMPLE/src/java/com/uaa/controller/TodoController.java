/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller;

import com.uaa.bean.helper.JBTodoItemManager;
import com.uaa.entity.TodoItem;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mario1
 */
@WebServlet(name = "TodoController", urlPatterns = {"/todo"})
public class TodoController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String viewToLoad = "view/";
        String action = request.getParameter("action");
        JBTodoItemManager beanManager = 
                JBTodoItemManager.getInstance();
        switch (action) {
            case "create":
                viewToLoad += "create_task.jsp";
                break;
            case "update":
                viewToLoad += "update_task.jsp";
                break;
            case "delete":
                viewToLoad += "delete_task.jsp";
                break;
            default:
                viewToLoad += "read_task.jsp";
                List<TodoItem> todoList = beanManager.read();
                request.setAttribute("todoList", todoList);
                break;
        }
        RequestDispatcher rd = request
                .getRequestDispatcher(viewToLoad);
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        JBTodoItemManager beanManager = JBTodoItemManager.getInstance();
        String action = request.getParameter("action");
        TodoItem item = getItemFromRequest(request);
        switch (action) {
            case "create":
                beanManager.create(item);
                break;
            case "update":
                beanManager.update(item);
                break;
            case "delete":
                beanManager.delete(item.getId());
                break;
            default:
                break;
        }
        List<TodoItem> todoList = beanManager.read();
        request.setAttribute("todoList", todoList);
        RequestDispatcher rd = request.getRequestDispatcher("view/read_task.jsp");
        rd.forward(request, response);
    }

    private TodoItem getItemFromRequest(HttpServletRequest request) {
        TodoItem item = new TodoItem();
        String id = request.getParameter("id");
        String task = request.getParameter("task");
        String description = request.getParameter("description");
        String completed = request.getParameter("completed");
        if (id != null) {
            item.setId(Integer.parseInt(id));
        }
        if (task != null) {
            item.setTask(task);
        }
        if (description != null) {
            item.setDescription(description);
        }
        if (completed != null) {
            item.setCompleted(true);
        }
        return item;

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
